/*jslint white: true */
var game = {
  paused : true,
  // invoked on page load
  start : function() {
    this.frameNo = 0;
    if (isMobile()) {
      gameArea.stop();
      return;
    };
    this.initAllTheThings();
    // set interval at which function updateGameArea is executed
    // 1000 ms divided by second parameter
    this.interval = setInterval(updateGameState, knobsAndLevers.intervalDivisor);
  },
  reset : function() {
    game.paused = true;
    gameArea.stop();
    players.clear();
    ledges.clear();
    this.start();
  },
  startNextFrame : function() {
    gameArea.clear();
    this.frameNo += 1;
  },
  initAllTheThings : function() {
    knobsAndLevers.init();
    gameArea.init();
    controls.init();
    texts.init();
  },
};

// this gets executed every interval
function updateGameState() {
  if (game.paused) {
    texts.updatePause();
    return;
  }
  game.startNextFrame();
  hud.update();
  ledges.manage();
  players.manage();
};
