var knobsAndLevers = {
  init : function() {
    this.players.startX = (this.canvas.width - bird.props.width) / 2;
    this.players.startY = this.canvas.height - bird.props.height;
    this.ledges.width = this.canvas.width / 4;
  },
  birds : {
    speed : 2,
    width : 30,
    height : 30,
    friction : 0.8,
    gravity : 0.1,
    maxSpeed : 4,
    directions : {
      left : -1,
      right : 1,
    },
  },
  ledges : {
    height : 10,
  },
  players : {
    amount : 4,
  },
  canvas : {
    width : 800,
    height : 800,
  },
  intervalDivisor : 5,
  colorOptions : ['blue', 'red', 'orange', 'green', 'black', 'grey', 'purple', 'pink', 'maroon'],
};
