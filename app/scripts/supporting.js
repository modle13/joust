/*jslint white: true */

function everyinterval(n) {
  if ((game.frameNo / n) % 1 === 0) {
    return true;
  };
  return false;
};

function getRandom(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

var colorsPicked = [];
function getRandomColor() {
  let colors = knobsAndLevers.colorOptions;
  let color = colors[Math.floor(Math.random() * colors.length)];
  if (inColorsPicked(color)) {
    color = getRandomColor();
  };
  return color;
};

function inColorsPicked(color) {
  for (i = 0; i < colorsPicked.length; i += 1) {
    if (colorsPicked[i] === color) {
      return true;
    };
  };
  return false;
};

function isMobile() {
  return ( location.search.indexOf( 'ignorebrowser=true' ) < 0 && /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test( navigator.userAgent ) );
};

function showMobile() {
  document.write("Mobile is not supported.<br><br>");
  document.write("The use of a keyboard is required.<br><br>");
  document.write("Sorry!<br><br>");
  document.write("To show how bad we feel, here's a gif so you can see what you're missing (that's not rude at all, we promise!)<br><br>");
  document.write("<img src='app/static/media/images/joust.gif' style='width: 100%;'></img>");
};

// TODO handle paused in controls; keys are already being captured
window.addEventListener('keydown', function (e) {
  if (e.keyCode == 32) {
    game.paused = !game.paused;
  }
});

// TODO handle reset in controls; keys are already being captured
window.addEventListener('keydown', function (e) {
  if (e.keyCode == 13) {
    game.reset()
  }
});
