/*jslint white: true */
var gameArea = {
  canvas : document.createElement("canvas"),
  init : function() {
    this.canvas.width = knobsAndLevers.canvas.width;
    this.canvas.height = knobsAndLevers.canvas.height;
    this.context = this.canvas.getContext("2d");
    this.draw();
  },
  draw : function() {
    document.body.insertBefore(this.canvas, document.body.childNodes[0]);
  },
  clear : function() {
    this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
  },
  stop : function() {
    clearInterval(this.interval);
  },
};
