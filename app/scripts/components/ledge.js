/*jslint white: true */
var ledges = {
  props : knobsAndLevers.ledges,
  ledges : [],
  manage : function() {
    if (game.frameNo == 1) {
      this.create();
    }
    this.update();
  },
  create : function() {
    let width = this.props.width;
    let height = this.props.height;
    let canvas = knobsAndLevers.canvas;
    // TODO refactor; so many repeated component details
    // look at centipede for usage of default component args
    // floor
    this.ledges.push(new component(canvas.width, height, "teal", 0, canvas.height - height, "ledge"));
    // middle
    this.ledges.push(new component(width, height, "teal", canvas.width / 2 - 2 * width / 3, 3 * canvas.height / 4, "ledge"));
    this.ledges.push(new component(width, height, "teal", canvas.width / 2 - 2 * width / 3, 2 * canvas.height / 4, "ledge"));
    this.ledges.push(new component(width, height, "teal", canvas.width / 2 - 2 * width / 3, canvas.height / 4, "ledge"));
    // left
    this.ledges.push(new component(width, height, "teal", -width / 2, canvas.height / 3 - canvas.height / 20, "ledge"));
    this.ledges.push(new component(width, height, "teal", -width / 2, 2 * canvas.height / 3 - canvas.height / 20, "ledge"));
    // right
    this.ledges.push(new component(width, height, "teal", canvas.width - width / 2, canvas.height / 3 - canvas.height / 20, "ledge"));
    this.ledges.push(new component(width, height, "teal", canvas.width - width / 2, 2 * canvas.height / 3 - canvas.height / 20, "ledge"));
  },
  update : function() {
    // TODO use forEach
    for (i = 0; i < this.ledges.length; i += 1) {
      this.ledges[i].update();
    };
  },
  clear : function() {
    this.ledges = [];
  },
};
